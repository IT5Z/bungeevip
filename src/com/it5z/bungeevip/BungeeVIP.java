package com.it5z.bungeevip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import com.it5z.bungeevip.manager.ConfigManager;
import com.it5z.bungeevip.manager.LoggerManager;
import com.it5z.bungeevip.manager.MySQLManager;

public class BungeeVIP extends JavaPlugin {
	private static BungeeVIP instance;
	private ConfigManager config;
	private ConfigManager lang;
	private LoggerManager logger;
	private MySQLManager mysql;
	
	@Override
	public void onEnable() {
		instance = this;
		config = new ConfigManager("config.yml");
		loadLang();
		updataConfig();
		loginMySQL();
		updataMySQL();
		logger = LoggerManager.instance;
		logger.sendConsoleMessage(lang.getString("插件加载成功消息", ChatColor.GREEN + "插件已被加载"));
	}
	
	@Override
	public void onDisable() {
		logger.sendConsoleMessage(lang.getString("插件卸载成功消息", ChatColor.GREEN + "插件已被卸载"));
	}
	
	public static BungeeVIP getInstance() {
		return instance;
	}
	
	public void loadLang() {
		lang = new ConfigManager("lang\\" + config.getString("语言", "zh_cn") + ".yml");
	}
	
	public void loginMySQL() {
		mysql = new MySQLManager(config.getString("MySQL数据库.地址", "localhost"), config.getInt("MySQL数据库.端口", 3306), config.getString("MySQL数据库.数据库名", "minecraft"), config.getString("MySQL数据库.用户名", "root"), config.getString("MySQL数据库.密码", "password"));
	}
	
	public void updataConfig() {
		config.set("修订版本", "0");
		lang.set("修订版本", "0");
	}
	
	public void updataMySQL() {
		try {
			PreparedStatement ps = mysql.getConnection().prepareStatement("SHOW TABLES LIKE ?;");
			Statement s = mysql.getConnection().createStatement();
			String prefix = config.getString("MySQL数据库.前缀", "vip_");
			ps.setString(1, prefix + "user");
			ResultSet rs = ps.executeQuery();
			if(! rs.next()) {
				s.executeUpdate("CREATE TABLE `" + prefix + "user`(`rowid` INT PRIMARY KEY AUTO_INCREMENT COMMENT '数据索引号', `name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称', `group` VARCHAR(100) NOT NULL COMMENT '所在组', `days` INT NOT NULL COMMENT '天数', `time` DATE NOT NULL COMMENT '时间')COMMENT '用户数据';");
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void reloadConfigManager() {
		config.Reload();
		loadLang();
		updataConfig();
		logger.loadPerfix();
		loginMySQL();
		updataMySQL();
	}
	
	public ConfigManager getConfigManager() {
		return config;
	}
	
	public ConfigManager getLang() {
		return lang;
	}
}
