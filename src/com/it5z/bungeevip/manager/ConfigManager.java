package com.it5z.bungeevip.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.it5z.bungeevip.BungeeVIP;

public class ConfigManager {
	File configfile;
	private FileConfiguration config;
	
	public ConfigManager(String filename) {
		configfile = new File(BungeeVIP.getInstance().getDataFolder(), filename);
		config = YamlConfiguration.loadConfiguration(configfile);
	}
	
	public String getString(String path, String def) {
		if(! config.isString(path)) {
			set(path, def);
			try {
				config.save(configfile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return config.getString(path, def);
	}
	
	public int getInt(String path, int def) {
		if(! config.isInt(path)) {
			config.set(path, def);
			try {
				config.save(configfile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return config.getInt(path, def);
	}
	
	public void set(String path, Object value) {
		config.set(path, value);
	}
	
	public void Reload() {
		try {
			config.load(configfile);
		} catch (FileNotFoundException e) {
			try {
				config.save(configfile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (InvalidConfigurationException | IOException e) {
			e.printStackTrace();
		}
	}
}
