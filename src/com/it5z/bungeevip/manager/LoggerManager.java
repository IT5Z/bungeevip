package com.it5z.bungeevip.manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;

import com.it5z.bungeevip.BungeeVIP;

/**
 * 
 * @author 孟祥昊
 * 后台信息管理类
 */
public class LoggerManager {
	/**
	 * 类的实例
	 */
	public static LoggerManager instance = new LoggerManager();
	/**
	 * 消息前缀
	 */
	private String messageprefix;
	/**
	 * 后台发送者
	 */
	private ConsoleCommandSender ccs;
	
	/**
	 * 后台信息管理类构造方法
	 */
	private LoggerManager() {
		loadPerfix();
		ccs = Bukkit.getConsoleSender();
	}
	
	public void loadPerfix() {
		messageprefix = BungeeVIP.getInstance().getLang().getString("消息前缀", "VIP");
	}
	
	/**
	 * 给后台发送信息
	 * @param message 要发送的信息
	 */
	public void sendConsoleMessage(String message) {
		ccs.sendMessage(ChatColor.YELLOW + "[" + messageprefix + ChatColor.YELLOW + "]" + ChatColor.RESET + " " + message);
	}
}
