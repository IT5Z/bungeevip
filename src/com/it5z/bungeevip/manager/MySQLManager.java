package com.it5z.bungeevip.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.ChatColor;

import com.it5z.bungeevip.BungeeVIP;

public class MySQLManager {
	private Connection connection;
	
	public MySQLManager(String host, int port, String dbname, String user, String password) {
		ConfigManager lang = BungeeVIP.getInstance().getLang();
		try {
			connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbname + "?user=" + user + "&password=" + password + "&useUnicode=true&characterEncoding=UTF-8");
			LoggerManager.instance.sendConsoleMessage(lang.getString("数据库连接成功消息", ChatColor.GREEN + "数据库连接成功"));
		} catch (SQLException e) {
			e.printStackTrace();
			LoggerManager.instance.sendConsoleMessage(lang.getString("数据库连接失败消息", ChatColor.GREEN + "数据库连接失败"));
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
}
